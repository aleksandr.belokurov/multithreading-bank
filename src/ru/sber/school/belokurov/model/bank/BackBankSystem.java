package ru.sber.school.belokurov.model.bank;

import ru.sber.school.belokurov.model.Request;
import ru.sber.school.belokurov.model.RequestType;

public class BackBankSystem {


    private volatile int amountMoneyInBank;
    private volatile int amountMoneyInDebtors = 0;
    private String name;

    public BackBankSystem(String name,int amountMoney) {
        this.name = name;
        this.amountMoneyInBank = amountMoney;
    }

    public synchronized void creditQuery(Request request) {
        if (request.getAmount() <= amountMoneyInBank) {
            this.amountMoneyInBank -= request.getAmount();
            this.amountMoneyInDebtors += request.getAmount();
            printInfo(request, true);
            sendResponse(request);
        } else {
            printInfo(request, false);
        }
    }

    public synchronized void repaymentQuery(Request request) {
        this.amountMoneyInBank += request.getAmount();
        this.amountMoneyInDebtors -= request.getAmount();
        printInfo(request, true);
        sendResponse(request);

    }

    private void sendResponse(Request request) {
        if (request.getRequestType().equals(RequestType.CREDIT)) {
            request.getClient().changeHaveAmountMoney(request.getAmount());
            request.getClient().changeOweAmountMoney(-request.getAmount());
        } else {
            request.getClient().changeHaveAmountMoney(-request.getAmount());
            request.getClient().changeOweAmountMoney(request.getAmount());
        }
    }


    public void printInfo(Request request, boolean isExecute) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Бэк система ").append(name);
        stringBuilder.append(request);
        if (isExecute) {
            stringBuilder.append(" выполнен");
        } else {
            stringBuilder.append(" не выполнен");
        }
        stringBuilder.append(" Общий баланс банка : ").append(amountMoneyInBank).
                append(" Колличество выданных денег : ").append(amountMoneyInDebtors);
        int generalAmount = amountMoneyInDebtors + amountMoneyInBank;
        stringBuilder.append(" Общий баланс с учетом задолжников : ").append(generalAmount);
        System.out.println(stringBuilder);
    }
}
