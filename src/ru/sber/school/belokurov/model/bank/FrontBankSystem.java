package ru.sber.school.belokurov.model.bank;

import ru.sber.school.belokurov.model.Request;

import java.util.Queue;

public class FrontBankSystem {

    private static final int MAX_ELEMENTS_IN_QUEUE = 2;

    private volatile Queue<Request> requestQueue;

    public FrontBankSystem(Queue<Request> requestQueue) {
        this.requestQueue = requestQueue;
    }

    public synchronized void addElementInQueue(Request request) {
        while (requestQueue.size() > MAX_ELEMENTS_IN_QUEUE - 1) {
            waiting();
        }
        requestQueue.add(request);
        notifyAll();
        request.getClient().setIWaitResponse(true);
        while (request.getClient().getIsWaitResponse()) {
            waiting();
        }
    }

    public synchronized Request pollElementQueue() {
        while (requestQueue.isEmpty()) {
            waiting();
        }
        Request request = requestQueue.poll();
        notifyAll();
        return request;
    }

    private synchronized void waiting() {
        try {
            wait();
        } catch (InterruptedException exception) {
            Thread.currentThread().interrupt();
            System.out.println("Ошибка, повторите попытку позже");
        }
    }

    public synchronized void releaseClient(Request request) {
        request.getClient().setIWaitResponse(false);
        notifyAll();

    }
}
