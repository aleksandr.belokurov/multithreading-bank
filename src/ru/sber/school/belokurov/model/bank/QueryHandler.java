package ru.sber.school.belokurov.model.bank;

import ru.sber.school.belokurov.model.Request;
import ru.sber.school.belokurov.model.RequestType;

public class QueryHandler implements Runnable {
    FrontBankSystem frontBankSystem;
    BackBankSystem backBankSystem;

    public QueryHandler(FrontBankSystem frontBankSystem, BackBankSystem backBankSystem) {
        this.frontBankSystem = frontBankSystem;
        this.backBankSystem = backBankSystem;
    }

    @Override
    public void run() {
        while (!Thread.currentThread().isInterrupted()) {
            Request request = frontBankSystem.pollElementQueue();
            printInfo(request);
            spendRequestInBank(request);
        }
    }

    private void spendRequestInBank(Request request) {
        if (request.getRequestType().equals(RequestType.CREDIT)) {
            backBankSystem.creditQuery(request);
        } else {
            backBankSystem.repaymentQuery(request);
        }
        try {
            Thread.sleep(5000);
        } catch (InterruptedException exception) {
            Thread.currentThread().interrupt();
            System.out.println("Ошибка, повторите попытку позже");
        }
        frontBankSystem.releaseClient(request);
    }

    private void printInfo(Request request) {

        System.out.println("Получена заявка на обработку " + request);
    }

}
