package ru.sber.school.belokurov.model.bank;

import java.util.ArrayList;
import java.util.List;

public class Bank {

    private FrontBankSystem frontBankSystem;
    private BackBankSystem backBankSystem;
    private List<QueryHandler> queryHandlers;

    public Bank(FrontBankSystem frontBankSystem, BackBankSystem backBankSystem, int numberQueryHandlers) {
        this.frontBankSystem = frontBankSystem;
        this.backBankSystem = backBankSystem;
        this.queryHandlers = new ArrayList<>();

        for (int i = 0; i < numberQueryHandlers; i++) {
            queryHandlers.add(new QueryHandler(this.frontBankSystem, this.backBankSystem));
        }
    }

    public FrontBankSystem getFrontBankSystem() {
        return frontBankSystem;
    }

    public BackBankSystem getBackBankSystem() {
        return backBankSystem;
    }

    public List<QueryHandler> getQueryHandlers() {
        return queryHandlers;
    }
}
