package ru.sber.school.belokurov.model;

public class Request {

    private Client client;
    private int amountMoney;
    private RequestType requestType;

    public Request(Client client, int amountMoney, RequestType requestType) {
        this.client = client;
        this.amountMoney = amountMoney;
        this.requestType = requestType;
    }

    public Client getClient() {
        return client;
    }

    public int getAmount() {
        return amountMoney;
    }

    public RequestType getRequestType() {
        return requestType;
    }

    @Override
    public String toString() {
        return "Request{" +
                "name='" + client.getName() + '\'' +
                ", amountMoney='" + amountMoney + '\'' +
                ", requestType=" + requestType +
                '}';
    }
}
