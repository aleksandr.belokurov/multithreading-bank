package ru.sber.school.belokurov.model;

public enum RequestType {
    CREDIT,
    REPAYMENT,
}
