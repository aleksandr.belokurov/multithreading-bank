package ru.sber.school.belokurov.model;

import ru.sber.school.belokurov.model.bank.Bank;

import java.util.List;
import java.util.Random;

public class Client implements Runnable{

    private static final int MAX_AMOUNT_MONEY_FOR_OPERATION = 400;
    private Random random = new Random();

    private String name;
    private List<Bank> bankList;
    private int haveAmountMoney = 0;
    private int oweAmountMoney = 0;
    private boolean isWaitResponse = false;

    public Client(String name, List<Bank> bankList) {
        this.name = name;
        this.bankList = bankList;
    }

    public String getName() {
        return name;
    }

    public void changeHaveAmountMoney(int haveAmountMoney) {
        this.haveAmountMoney += haveAmountMoney;
    }

    public boolean getIsWaitResponse() {
        return isWaitResponse;
    }

    public void setIWaitResponse(boolean bol) {
        isWaitResponse = bol;
    }

    public void changeOweAmountMoney(int oweAmountMoney) {
        this.oweAmountMoney += oweAmountMoney;
    }

    @Override
    public void run() {
        while (!Thread.currentThread().isInterrupted()) {
            printInfo();
            if (random.nextBoolean()) {
                creditRequest();
            } else {
                repaymentRequest();
            }
        }
    }


    private void creditRequest(){
        Request request = new Request(this, random.nextInt(MAX_AMOUNT_MONEY_FOR_OPERATION), RequestType.CREDIT);
        sendRequest(request);
    }

    private void repaymentRequest(){
        int amountMoneyForOperation = random.nextInt(MAX_AMOUNT_MONEY_FOR_OPERATION);
        Request request = new Request(this, amountMoneyForOperation, RequestType.REPAYMENT);
        if (amountMoneyForOperation > haveAmountMoney) {
            request = new Request(this, amountMoneyForOperation, RequestType.CREDIT);
        }
        sendRequest(request);
    }

    private void sendRequest(Request request) {
        int randomBank = random.nextInt(bankList.size());
        Bank bank = bankList.get(randomBank);
        bank.getFrontBankSystem().addElementInQueue(request);
    }

    private void printInfo() {
        System.out.println(this + " отправил запрос.");
    }

    @Override
    public String toString() {
        return "Client{" +
                "name='" + name + '\'' +
                ", haveAmountMoney=" + haveAmountMoney +
                ", oweAmountMoney=" + oweAmountMoney +
                '}';
    }
}
