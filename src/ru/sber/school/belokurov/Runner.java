package ru.sber.school.belokurov;

import ru.sber.school.belokurov.controller.Controller;

public class Runner {


    public static void main(String[] args) {
        Controller controller = new Controller();
        controller.start();
    }
}
