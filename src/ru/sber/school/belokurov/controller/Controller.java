package ru.sber.school.belokurov.controller;

import ru.sber.school.belokurov.model.Client;
import ru.sber.school.belokurov.model.bank.BackBankSystem;
import ru.sber.school.belokurov.model.bank.Bank;
import ru.sber.school.belokurov.model.bank.FrontBankSystem;
import ru.sber.school.belokurov.model.bank.QueryHandler;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Controller {

    private static final int NUMBER_CLIENTS = 6;
    private static final int NUMBER_BANKS = 2;
    private static final int NUMBER_HANDLERS = 2;
    private static final int AMOUNT_MONEY_IN_BANK = 2000;


    private List<Client> clientList;
    private List<Bank> bankList;


    public void start() {
        initialization();
        List<Thread> clientThreadList = createListThread(clientList);
        List<QueryHandler> queryHandlers = new ArrayList<>();
        for (Bank bank : bankList) {
            queryHandlers.addAll(bank.getQueryHandlers());
        }
        List<Thread> handlerThreadList = createListThread(queryHandlers);
        clientThreadList.forEach(Thread::start);
        handlerThreadList.forEach(thread -> thread.setDaemon(true));
        handlerThreadList.forEach(Thread::start);

    }


    private void initialization() {
        this.bankList = createBanks();
        this.clientList = createClients(bankList);

    }

    private List<Thread> createListThread(List<? extends Runnable> listEntities) {
        List<Thread> threadList = new ArrayList<>();
        for (Runnable listEntity : listEntities) {
            threadList.add(new Thread(listEntity));
        }
        return threadList;
    }

    private List<Client> createClients(List<Bank> bankList) {
        List<Client> clients = new ArrayList<>();
        for (int i = 0; i < NUMBER_CLIENTS; i++) {
            clients.add(new Client("Client" + i, bankList));
        }
        return clients;
    }

    private List<Bank> createBanks() {
        List<Bank> banks = new ArrayList<>();
        for (int i = 0; i < NUMBER_BANKS; i++) {
            banks.add(new Bank(new FrontBankSystem(new LinkedList<>()), new BackBankSystem("Bank " + i, AMOUNT_MONEY_IN_BANK), NUMBER_HANDLERS));
        }
        return banks;
    }
}
